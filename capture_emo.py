# updated code for processing 

# face detection uses HAAR Cascades
# emotion detection uses pre-trained model from fer2013 dataset:
# https://medium.com/@birdortyedi_23820/deep-learning-lab-episode-3-fer2013-c38f2e052280


from statistics import mode

import os
import cv2
import sys
import numpy as np
from keras.models import load_model

from utils.datasets import get_labels
from utils.inference import detect_faces
from utils.inference import draw_text
from utils.inference import draw_bounding_box
from utils.inference import apply_offsets
from utils.inference import load_detection_model
from utils.preprocessor import preprocess_input

def DifferentEnough(image_a, image_b):
    # image comparison (scene change detection) here
    ###convert you images to grayscale

    gray_a = cv.cvtColor(image_a, cv.COLOR_BGR2GRAY)
    gray_b = cv.cvtColor(image_b, cv.COLOR_BGR2GRAY)

    # compute the similarity and difference
    (score, diff) = compare_ssim(gray_a, gray_b, full=True)
    diff = (diff * 255).astype("uint8")

    #print("Frame similarity: {}".format(score))
    if score < scene_thr:
        # scenes are dissimilar (different enough)
        return True 
    else:
        # scenes are similar (not different enough)
        return False

# starting video streaming
cv2.namedWindow('window_frame')

# determine whether to capture from file or from webcam
if len(sys.argv) > 1:
    fn = sys.argv[1]
    if os.path.exists(fn):
        print ("Reading from", os.path.basename(fn))
    else:
        print ("Can't find", os.path.basename(fn))
        quit()
else:
    print("No video filename specified. Will attempt to read from camera.")
    fn = 0

# crerate a video capture device with specified input
video_capture = cv2.VideoCapture(fn)

# prepare models for detection.
# parameters for loading data and images
detection_model_path = 'data/trained_models/detection_models/haarcascade_frontalface_default.xml'
emotion_model_path = 'data/trained_models/emotion_models/fer2013_mini_XCEPTION.102-0.66.hdf5'
emotion_labels = get_labels('fer2013')

# hyper-parameters for bounding boxes shape
frame_window = 10
emotion_offsets = (20, 40)

# loading models
face_detection = load_detection_model(detection_model_path)
emotion_classifier = load_model(emotion_model_path, compile=False)

# getting input model shapes for inference
emotion_target_size = emotion_classifier.input_shape[1:3]

# starting lists for calculating modes
emotion_window = []


while True:
    bgr_image = video_capture.read()[1]
    gray_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2GRAY)
    rgb_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2RGB)
    faces = detect_faces(face_detection, gray_image)

    for face_coordinates in faces:

        x1, x2, y1, y2 = apply_offsets(face_coordinates, emotion_offsets)
        gray_face = gray_image[y1:y2, x1:x2]
        try:
            gray_face = cv2.resize(gray_face, (emotion_target_size))
        except:
            continue

        gray_face = preprocess_input(gray_face, True)
        gray_face = np.expand_dims(gray_face, 0)
        gray_face = np.expand_dims(gray_face, -1)
        emotion_prediction = emotion_classifier.predict(gray_face)
        emotion_probability = np.max(emotion_prediction)
        emotion_label_arg = np.argmax(emotion_prediction)
        emotion_text = emotion_labels[emotion_label_arg]
        emotion_window.append(emotion_text)

        if len(emotion_window) > frame_window:
            emotion_window.pop(0)
        try:
            emotion_mode = mode(emotion_window)
        except:
            continue

        if emotion_text == 'angry':
            color = emotion_probability * np.asarray((255, 0, 0))
        elif emotion_text == 'sad':
            color = emotion_probability * np.asarray((0, 0, 255))
        elif emotion_text == 'happy':
            color = emotion_probability * np.asarray((255, 255, 0))
        elif emotion_text == 'surprise':
            color = emotion_probability * np.asarray((0, 255, 255))
        else:
            color = emotion_probability * np.asarray((0, 255, 0))

        color = color.astype(int)
        color = color.tolist()

        draw_bounding_box(face_coordinates, rgb_image, color)

        draw_text(face_coordinates, rgb_image, emotion_mode, color, 0, -45, 1, 1)
        #cv2.putText(rgb_image, emotion_text, (15,15), cv2.FONT_HERSHEY_PLAIN, 1.5, (0, 255, 0), 2)

    bgr_image = cv2.cvtColor(rgb_image, cv2.COLOR_RGB2BGR)
    cv2.imshow('window_frame', bgr_image)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

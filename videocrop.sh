#!/bin/bash
# Usage: ./videocrop.sh
# to preview the effects, use ffplay:
# ffplay -i kevin_phone_prank_reaction.mp4 -vf "crop=161:100:1207:652"
# cropping example: 
# ffmpeg -i kevin_phone_prank_reaction.mp4 -vf "crop=161:110:1207:662" -c:a copy _kevin_phone_prank_reaction.mp4

mkdir cropped
for file in *.mp4;
do
    ffmpeg -i "${file}" -vf crop=161:110:1207:662 -c:a copy "./cropped/$file"
done
# New version. Purpose: get a frame of video, run Rekognition object detection (detect_labels), save results as JSON.
# uses AWS SDK for Python (Boto3): https://aws.amazon.com/sdk-for-python/
# Sanity check: should not have more than 10,000 frames per video (that's 3-hr video sampled every second) to analyze.

import json
import boto3
import botocore

AWSBUCKET = "mldata.tailoredlabs.org"
AWSREGION = "us-east-1"
FRAMEPATH = "video_frames/%x_%y/%x_%y-%z.jpg"

# object (label) detection using AWS Rekognition
def detect_labels(bucket, key, attributes=['ALL'], region=AWSREGION):
	rekognition = boto3.client("rekognition", region)
	response = rekognition.detect_labels(
		Image={
			"S3Object": {
				"Bucket": bucket,
				"Name": key,
			}
		},
		#Attributes=attributes,
	)
	return response

s3 = boto3.resource('s3') # instantiate S3 access

for student in range(1,7):
	for course in range(1,11):
		framecounter = 1
		# loop through frames until an error is encountered, or upper limit is reached
		while True:
			try:
				frame = FRAMEPATH
				frame = frame.replace("%x", str(student)) # susbsitute student id
				frame = frame.replace("%y", str(course)) # substitute course id
				frame = frame.replace("%z", str(framecounter)) # substitute frame counter
				#print("frame:", frame)

				s3.Object(AWSBUCKET, frame).load()
				framecounter = framecounter + 1
				if framecounter > 10000: # sanity check
					print("Too many frames per video - check your settings.")
					break
			except botocore.exceptions.ClientError as e:
				if e.response['Error']['Code'] == "404": # The frame does not exist.
					print("Ran out of frames, stopping.")
					break
				else:
					raise
			else: 
				# The frame is accessible
				print("detecting labels:", frame)
				labeldata = detect_labels(AWSBUCKET, frame)
				framejsonfile = ""

				# save JSON as a file
				framejsonfile = frame.replace(".jpg", ".labels.json")
				print("writing labels to JSON file:", framejsonfile)
				jsonobject = s3.Object(AWSBUCKET, framejsonfile)
				jsonobject.put(Body=(bytes(json.dumps(labeldata).encode('UTF-8'))))

				#s3.put_object(Body=facedata, Bucket=AWSBUCKET, Key=framejsonfile)
				#print(facedata)



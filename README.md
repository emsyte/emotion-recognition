This project will provide face detection, eye detection, and emotion recognition capabilities.  Future updates will include an ability to build classifiers
for determining Engagement and Attention scores using logistic regression.

References:

https://medium.com/@birdortyedi_23820/deep-learning-lab-episode-3-fer2013-c38f2e052280
https://github.com/ZhaoJ9014/face.evoLVe.PyTorch
https://ai.google/research/pubs/pub46555
https://medium.com/tensorflow/introducing-tensorflow-graphics-computer-graphics-meets-deep-learning-c8e3877b7668
https://github.com/thoughtworksarts/EmoPy


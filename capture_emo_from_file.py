# This will use a video file as input.

import cv2
import numpy as np

def DifferentEnough(image_a, image_b):
	# image comparison (scene change detection) here
	###convert you images to grayscale

	gray_a = cv.cvtColor(image_a, cv.COLOR_BGR2GRAY)
	gray_b = cv.cvtColor(image_b, cv.COLOR_BGR2GRAY)

	# compute the similarity and difference
	(score, diff) = compare_ssim(gray_a, gray_b, full=True)
	diff = (diff * 255).astype("uint8")

	#print("Frame similarity: {}".format(score))
	if score < scene_thr:
		# scenes are dissimilar (different enough)
		return True 
	else:
		# scenes are similar (not different enough)
		return False

 
# Create a VideoCapture object and read from input file
# If the input is the camera, pass 0 instead of the video file name
cap = cv2.VideoCapture('video/_kevin_phone_prank_reaction.mp4')
 
# check if camera is working
if (cap.isOpened()== False): 
  print("Error opening video stream or file")

# upon launch, we don't yet have an image to compare next scene to
first_run = True
 
# read until done
while(cap.isOpened()):
  # capture frame-by-frame
  ret, frame = cap.read()
  if ret == True:
 
    # display current frame
    cv2.imshow('Frame',frame)
 
    # press Q to  exit
    if cv2.waitKey(25) & 0xFF == ord('q'):
      break
 
  # Break the loop
  else: 
    break
 
# When everything done, release the video capture object
cap.release()
 
# Closes all the frames
cv2.destroyAllWindows()


# MVA 
# capture video, extract frames, detect faces

import cv2

from keras.models import load_model
import numpy as np

from utils.datasets import get_labels
from utils.inference import detect_faces
from utils.inference import draw_text
from utils.inference import draw_bounding_box
from utils.inference import apply_offsets
from utils.inference import load_detection_model
from utils.preprocessor import preprocess_input


# our HAAR cascades for face and eye detection
faceCascade = cv2.CascadeClassifier('data/haarcascades/haarcascade_frontalface_alt.xml')
eye_cascade = cv2.CascadeClassifier('data/haarcascades/haarcascade_eye.xml')

# pre-trained emotion detection model from https://github.com/oarriaga/face_classification
emotion_model_path = 'data/trained_models/emotion_models/fer2013_mini_XCEPTION.102-0.66.hdf5'
emotion_labels = get_labels('fer2013')

# hyper-parameters for bounding boxes shape
frame_window = 10
emotion_offsets = (20, 40)

# loading emotion model
emotion_classifier = load_model(emotion_model_path, compile=False)

# getting input model shapes for inference
emotion_target_size = emotion_classifier.input_shape[1:3]

# starting lists for calculating modes
emotion_window = []


def show_videofeed(mirror=False):
    webcam = cv2.VideoCapture(0)
    while True:
        ret_val, img = webcam.read()
        if mirror: 
            img = cv2.flip(img, 1)

        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=5,
            minSize=(30, 30),
            flags = cv2.CASCADE_SCALE_IMAGE
        )

        # for each face, draw a rectangle around it
        for (x, y, w, h) in faces:
            cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 2)
            # find eyes
            roi_gray = gray[y:y + h, x:x + w]
            eyes = eye_cascade.detectMultiScale(roi_gray)
            for (ex, ey, ew, eh) in eyes:
            	cv2.rectangle(img, (x + ex, y + ey), (x + ex+ew, y + ey+eh), (255, 0, 0), 2)



        cv2.imshow('viewer feed', img)
        if cv2.waitKey(1) == 27: 
            break  # press Esc to close
    cv2.destroyAllWindows()

def main():
    show_videofeed(mirror=False)

if __name__ == '__main__':
    main()

